#!/usr/bin/env nodejs

var conll = require( "./conll" );
var gce = require( "./gce" );

var asText = 1;

function printHelp( )
{
    console.log( "Produz forma legivel alternativa para arquivo JSON de saida do processamento CGE (Google Cloud Engine).\n" +
    "2text {params} arquivo\n" +
    "em {params}:\n" +
    "Informe \"--text\" ou \"--type text\" para saida em texto; (DEFAULT)\n" +
    "  ou\n" +
    "Informe \"--conll\" ou \"--type conll\" para saida em CONLL-U." );
}

var nArg;

for( nArg = 2; nArg < process.argv.length; ++nArg ) {
    switch( process.argv[ nArg ] ) {
    case '--text':
        asText= 1;
        break;

    case '--conll':
        asText = 0;
        break;

    case '--type':
        if( ++nArg >= process.argv.length ) {
            console.log( "O parametro '--type' necessita argumento 'text' ou 'conll'." );
            process.exit( 1 );
        }
        switch( process.argv[ nArg ] ) {
        case 'text':
            asText = 1;
            break;

        case 'conll':
            asText = 0;
            break;

        default:
            console.log( "Tipo '%s' invalido.", process.argv[ nArg ] );
            process.exit( 2 );
        }
        break;

    case '--help':
    case '-h':
    case '-?':
        printHelp( );
        process.exit( 0 );
        break;

    default:
        fName = process.argv[ nArg ];
        conteudo = gce.load( fName );
        if( asText )
            conll.dumpText( conteudo );
        else
            conll.dumpCONLL( fName, conteudo );
        break;
    }
}