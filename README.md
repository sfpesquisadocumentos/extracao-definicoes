# extracao-definicoes

Código-fonte apêndice do artigo **"Exemplo de Extração de Definições em Textos Articulados de Normas Jurídicas com o apoio do Processamento de Linguagem Natural"**, de Wagner Rodrigues Teixeira, et. al.

Trabalho produzido pelo Grupo de Estudos e Pesquisas Acadêmicas do Instituto Legislativo Brasileiro do Senado Federal (ILB/SF), edital COESUP 004/2016.

Material referenciado em https://doi.org/10.6084/m9.figshare.7624604.v1

## Orientações

### Filtro 0

O arquivo `finddefs0.sh` pode ser utilizado para filtrar o repositório de sentenças no formato texto utilizando expressões regulares para selecionar todas as sentenças que possuam 
"é o" e suas variações de gênero e número na terceira pessoa do indicativo. O resultado da chamada ao filtro deve produzir a primeira lista de referências às sentenças, no arquivo `defs0.v1.txt`.


```bash
./finddefs0.sh | sort > defs0.v1.txt
```

### Teste de sentença processada pelo Google Compute Engine

Arquivo `filterdefs0.sh` deve ser chamado da forma:

```bash
./filterdefs0.sh > defs0.v3.txt
```

### Filtro para separação das sentenças autorizativas

Exemplos de utilização:

ara excluir autorizativos do grupo principal, cuja versão no _pipeline_ passa se chamar `defs0.v4.txt`:

```bash
./filterautorizativos.sh --nomatch > defs0.v4.txt
```

Para produzir o grupo das leis autorizativas e guardá-lo no arquivo `defs0.v3-autorizativas.txt`:

```bash
./filterautorizativos.sh --match > defs0.v3-autorizativas.txt
```

### Identificação de definições de entidades

Exemplos de utilização:

Para excluir do conjunto resultante as sentenças que definem entidades e guardar o novo conjunto no arquivo `defs0.v5.txt`:

```bash
./filterentities.sh --nomatch > defs0.v5.txt
```

Para criar um grupo das sentenças que definem entidades a partir do conjunto atualmente selecionado:

```bash
./filterentities.sh --match > defs0.v4-entidades.txt
```