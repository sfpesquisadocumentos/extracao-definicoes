#!/usr/bin/env bash

# uso: $(basename ${0}) < database

database="defs0.v4.txt"
first=1

help( )
{
    echo "$(basename ${0}) -- navega uma base de JSON mostrados como CONLL resumido (colunas 1, 2, 4, 7 e 8)"
    echo "Parametros:"
    echo "--db {arquivo} - arquivo contendo caminhos para sentencas-txt"
    echo "        default: \"${database}\""
    echo "--first {N} - linha a partir da qual iniciar a navegacao"
    echo "        default: ${first}"
    echo "--help ou -h ou -? - mostra esta mensagem de ajuda"
}

prompt( )
{
    _prompted="x"
    if [ "${#}" -ge "1" ]
    then
        while :
        do
            echo -n "${1}"
            read r
            if [ -z "${r}" ]
            then
                _prompted="${3}"
                return
            fi
            if grep ":${r}:" <<< "${2}" 2>&1 > /dev/null
            then
                _prompted="${r}"
                return
            fi
        done
    fi
}

while [ "${#}" -gt "0" ]
do
    case "${1}" in
    "--db")
        shift
        database="${1}"
        ;;
    "--first")
        shift
        first="${1}"
        ;;
    "--help"|"-h"|"-?")
        help
        exit 1
        ;;
    *)
        echo "Parametro  \"${1}\" incorreto."
        help
        exit 2
        ;;
    esac
    shift
done

for (( line = first, lastline = $(wc -l < ${database}); line <= lastline; ++line ))
do
    sentencefile=$(sed -n "${line}p" "${database}")
    jsonfile=$(./pathtxt2json.sh <<< ${sentencefile} )
    _prompted="R"
    while :
    do
        case "${_prompted}" in
        [Pp])
            break
            ;;
        [Aa])
            if [ "${line}" -gt "1" ]
            then
                let line-=2
            else
                let --line
            fi
            break
            ;;
        [Rr])
            ./2text.js --conll "${jsonfile}" | cut -d $'\t' -f1,2,4,7,8 | less
            ;;
        [Jj])
            less "${jsonfile}"
            ;;
        [Cc])
            break 2
            ;;
        esac
        prompt "${line}/${lastline} - [P]roximo, [A]nterior, [R]ever, ver [J]SON, [C]ancelar: (P)" ":P:p:A:a:R:r:J:j:C:c:" "P"
    done
done