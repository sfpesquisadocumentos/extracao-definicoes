#!/usr/bin/env nodejs

var util = require( 'util' );
var sprintf = util.format;

var _self_module = {
    getFORM: function( token )
    {
        return( token.text.content );
    },

    getLEMMA: function( token )
    {
        if( 'lemma' in token )
            return( token.lemma );
        return( '_' );
    },

    getUPOSTAG: function( token )
    {
        return( token.partOfSpeech.tag );
    },

    getXPOSTAG: function( token )
    {
        if( _self_module.getUPOSTAG( token ) == 'PUNCT' )
            return( '.' );
        return( '_' );
    },

    /*
    mapFEAT_animacy: function( pos )
    {
        return( undefined );
    },
    */

    mapFEAT_aspect: function( pos )
    {
        var aspectMap = {
            'IMPERFECTIVE': 'Imp',
            'PERFECTIVE': 'Perf'
        };

        if( ! ( 'aspect' in pos ) || ! ( pos.aspect in aspectMap ) )
            return( undefined );
        return( 'Aspect=' + aspectMap[ pos.aspect ] );
    },

    mapFEAT_case: function( pos )
    {
        var caseMap = {
            'ACCUSATIVE': 'Acc',
            'DATIVE': 'Dat',
            'NOMINATIVE': 'Nom',
    //        'PREPOSITIONAL': ''
        };

        if( ! ( 'case' in pos ) || ! ( pos.case in caseMap ) )
            return( undefined );
        return( 'Case=' + caseMap[ pos.case ] );
    },
    /*
    mapFEAT_definite: function( pos )
    {
        return( undefined );
    },

    mapFEAT_degree: function( pos )
    {
        return( undefined );
    },
    */
    mapFEAT_gender: function( pos )
    {
        var genderMap = {
            'FEMININE': 'Fem',
            'MASCULINE': 'Masc',
            'COMMON': 'Common',
            'NEUTER': 'Neut'
        };

        if( ! ( 'gender' in pos ) || ! ( pos.gender in genderMap ) )
            return( undefined );
        return( 'Gender=' + genderMap[ pos.gender ] );
    },

    mapFEAT_mood: function( pos )
    {
        var moodMap = {
            'IMPERATIVE': 'Imp',
            'INDICATIVE': 'Ind',
            'SUBJUNCTIVE': 'Sub',
        };

        if( ! ( 'mood' in pos ) || ! ( pos.mood in moodMap ) )
            return( undefined );
        return( 'Mood=' + moodMap[ pos.mood ] );
    },
    /*
    mapFEAT_negative: function( pos )
    {
        return( undefined );
    },

    mapFEAT_numtype: function( pos )
    {
        return( undefined );
    },
    */
    mapFEAT_number: function( pos )
    {
        var numberMap = {
            'PLURAL': 'Plur',
            'SINGULAR': 'Sing'
        };

        if( ! ( 'number' in pos ) || ! ( pos.number in numberMap ) )
            return( undefined );
        return( 'Number=' + numberMap[ pos.number ] );
    },

    mapFEAT_person: function( pos )
    {
        var personMap = {
            'FIRST': '1',
            'SECOND': '2',
            'THIRD': '3'
        };

        if( ! ( 'person' in pos ) || ! ( pos.person in personMap ) )
            return( undefined );
        return( 'Person=' + personMap[ pos.person ] );
    },

    /*
    mapFEAT_poss: function( pos )
    {
        return( undefined );
    },

    mapFEAT_prontype: function( pos )
    {
        return( undefined );
    },

    mapFEAT_reflex: function( pos )
    {
        return( undefined );
    },
    */

    mapFEAT_tense: function( pos )
    {
        var tenseMap = {
    //        'CONDITIONAL_TENSE',
            'FUTURE': 'Fut',
            'PAST': 'Past',
            'PLUPERFECT': 'Pqp',
            'PRESENT': 'Pres',
        };

        if( ! ( 'tense' in pos ) || ! ( pos.tense in tenseMap ) )
            return( undefined );
        return( 'Tense=' + tenseMap[ pos.tense ] );
    },

    /*
    mapFEAT_verbform: function( pos )
    {
        return( undefined );
    },

    mapFEAT_voice: function( pos )
    {
        return( undefined );
    },
    */

    getFEATS: function( token )
    {
        var feats, pos;
        var featArray;

    // Animacy: animacy
    // Aspect: aspect
    // Case: case
    // Definite: definiteness or state
    // Degree: degree of comparison
    // Gender: gender
    // Mood: mood
    // Negative: whether the word can be or is negated
    // NumType: numeral type
    // Number: number
    // Person: person
    // Poss: possessive
    // PronType: pronominal type
    // Reflex: reflexive
    // Tense: tense
    // VerbForm: form of verb or deverbative
    // Voice: voice

        pos = token.partOfSpeech;
        featArray = new Array( );
    //    featArray.push( _self_module.mapFEAT_animacy( pos ) );
        featArray.push( _self_module.mapFEAT_aspect( pos ) );
        featArray.push( _self_module.mapFEAT_case( pos ) );
    //    featArray.push( _self_module.mapFEAT_definite( pos ) );
    //    featArray.push( _self_module.mapFEAT_degree( pos ) );
        featArray.push( _self_module.mapFEAT_gender( pos ) );
        featArray.push( _self_module.mapFEAT_mood( pos ) );
    //    featArray.push( _self_module.mapFEAT_negative( pos ) );
    //    featArray.push( _self_module.mapFEAT_numtype( pos ) );
        featArray.push( _self_module.mapFEAT_number( pos ) );
        featArray.push( _self_module.mapFEAT_person( pos ) );
    //    featArray.push( _self_module.mapFEAT_poss( pos ) );
    //    featArray.push( _self_module.mapFEAT_prontype( pos ) );
    //    featArray.push( _self_module.mapFEAT_reflex( pos ) );
        featArray.push( _self_module.mapFEAT_tense( pos ) );
    //    featArray.push( _self_module.mapFEAT_verbform( pos ) );
    //    featArray.push( _self_module.mapFEAT_voice( pos ) );
        feats = "";
        featArray.forEach( function( o, i ) {
            if( o && o != undefined && o.length > 0 ) {
                if( feats.length > 0 )
                    feats += '|';
                feats += o;
            }
        } );
        return( feats.length > 0 ? feats : '_' );
    },

    getHEAD: function( token )
    {
        if( token.dependencyEdge.label == 'ROOT' )
            return( '0' );
        return( 1 + parseInt( token.dependencyEdge.headTokenIndex ) );
    },

    getDEPREL: function( token )
    {
        return( token.dependencyEdge.label );
    },

    getDEPS: function( token )
    {
        return( '_' );
    },

    getMISC: function( token )
    {
        return( '_' );
    },

    token2Conllu: function( tokenSeq, token )
    {
        // ID, FORM, LEMMA, UPOSTAG, XPOSTAG, FEATS, HEAD, DEPREL, DEPS, MISC
        return( sprintf( '%d\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s',
            tokenSeq + 1,
            _self_module.getFORM( token ),
            _self_module.getLEMMA( token ),
            _self_module.getUPOSTAG( token ),
            _self_module.getXPOSTAG( token ),
            _self_module.getFEATS( token ),
            _self_module.getHEAD( token ),
            _self_module.getDEPREL( token ),
            _self_module.getDEPS( token ),
            _self_module.getMISC( token ) ) );
    },

    getText: function( o, first, last )
    {
        var t;
        var txt;

        if( first === undefined )
            first = 0;
        if( last === undefined || last < 1 )
            last = o.tokens.length;

        txt = "";
        for( t = first; t < last; ++t ) {
            if( txt.length > 0 )
                txt += " ";
            txt += o.tokens[ t ].text.content;
        }
        return( txt );
    },

    dumpText: function( o, first, last )
    {
        console.log( _self_module.getText( o, first, last ) );
    },

    getCONLL: function( fName, o )
    {
        var text, tokenSeq;

        text = sprintf( "# %s\n", fName );
        o.tokens.forEach( function( o, i ) {
            text += _self_module.token2Conllu( i, o ) + "\n";
        } );
        return( text );
    },

    dumpCONLL: function( fName, o )
    {
        console.log( _self_module.getCONLL( fName, o ) );
    },

    dumpToken: function( token, tokenSeq )
    {
        if( token.partOfSpeech.tag == 'VERB' )
            console.log( '%d: %s: (->%d) %s %s %s %s %s',
                tokenSeq + 1,
                token.text.content,
                token.dependencyEdge.headTokenIndex,
                token.dependencyEdge.label,
                token.partOfSpeech.tag,
                token.lemma,
                token.partOfSpeech.tense,
                token.partOfSpeech.person );
        else
            console.log( '%d: %s: (->%d) %s %s',
                tokenSeq + 1,
                token.text.content,
                token.dependencyEdge.headTokenIndex,
                token.dependencyEdge.label,
                token.partOfSpeech.tag );
    }
}

module.exports = _self_module;