#!/usr/bin/env bash

#
# comando para localizar sentencas de leis autorizativas
#
# como o repositorio de leis contem tambem projetos, e' necessario
# que se faca um filtro que execute a pesquisa apenas nos textos das leis

verbose=0
database="defs0.v3.txt"
firstline=1
linecount=0
match=1

help( )
{
    echo "$(basename ${0}):"
    echo "--match - gera saida com as sentencas autorizativas"
    echo "--nomatch - gera saida com as sentencas que nao sao autorizativas (default)"
    echo "--db {arquivo com lista das sentencas} - os arquivos apontados devem"
    echo "            conter as sentencas em txt originadoras das base JSON."
    echo "            Default: \"${database}\""
    echo "--start {n} - linha inicial de processamento [1..n]"
    echo "            Default: \"${firstline}\""
    echo "--count {n} - quantidade de linhas a serem processadas (0 = todas)"
    echo "            Default: \"${linecount}\""
    echo "--verbose - habilita apresentacao de mensagens, que serao precedidas"
    echo "            por \"* \""
    echo "--noverbose - desliga opcao \"--verbose\""
    echo "--help ou -h ou -? - mostra mensagem de ajuda."
}

while [ "${#}" -ge 1 ]
do
    case "${1}" in
    "--match")
        match=1
        ;;
    "--nomatch")
        match=0
        ;;
    "--db")
        shift
        database="${1}"
        ;;
    "--start")
        shift
        firstline="${1}"
        ;;
    "--count")
        shift
        linecount="${1}"
        ;;
    "--verbose")
        verbose=1
        ;;
    "--noverbose")
        verbose=0
        ;;
    "--help"|"-h"|"-?"|*)
        help
        exit 0
        ;;
    esac
    shift
done

totallines=$(wc -l < "${database}")
lastline=${totallines}
if [ "${linecount}" -gt "0" ]
then
    lastline=$((firstline + linecount - 1))
fi

currentline="${firstline}"

if [ "${verbose}" -a "${verbose}" -gt "0" ]
then
    echo "* Avaliando listagem \"${database}\""
    echo "* Quantidade de arquivos: ${totallines}"
    echo "* Arquivo inicial: ${currentline}"
    echo "* Quantidade de arquivos: $(( lastline - currentline + 1 ))"
    echo "* lastline=${lastline}, currentline=${currentline}"
fi

while [ "${currentline}" -le "${lastline}" ]
do
    sentencefile=$(sed -n "${currentline}p" "${database}")
    jsonfile=$(./pathtxt2json.sh <<< ${sentencefile} )
    if [ "${verbose}" -a "${verbose}" -gt "0" ]
    then
        echo "* arquivo corrente: ${currentline}"
        echo "* sentencefile=${sentencefile}"
        echo "* jsonfile=${jsonfile}"
    fi
    if ./isautorizativo.js ${jsonfile}
    then
        isaut=1
    else
        isaut=0
    fi
    let doecho=isaut^\!match
    if [ "${doecho}" -ne "0" ]
    then
        echo "${sentencefile}"
    fi
    let ++currentline
done