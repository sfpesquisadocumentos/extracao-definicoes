#!/usr/bin/env bash

# comando para localizar os padr�es de sequencia de string
# basicos para o criterio 0 - caracteristicas morfossintaticas.
#
# O termo central e' o verbo SER seguido de artigo.
#

# como o repositorio de leis contem tambem projetos, e' necessario
# que se faca um filtro que execute a pesquisa apenas nos textos das leis

BASE=/opt/normas/txt-sentencas
verbose=0
ano=""

help( )
{
    echo "$(basename ${0}):"
    echo "--ano {ano} - ano com 4 digitos. Default todos"
    echo "--verbose - mostra o arquivo entre colchetes sozinho na linha e nas linhas abaixo o conteudo"
    echo "--noverbose - mostra apenas o nome do arquivo - default"
}

while [ "${#}" -ge 1 ]
do
    case "${1}" in
    --ano)
        shift
        ano="${1}"
        ;;
    --verbose)
        verbose=1
        ;;
    --noverbose)
        verbose=0
        ;;
    --help|-h|-?|*)
        help
        exit 0
        ;;
    esac
    shift
done

find "${BASE}/${ano}" -type d -name "LEI*" -print | while read lei
do
    for f in ${lei}/*
    do
        if egrep "(( � (o|a|um|uma) )|( s�o (os|as|uns|umas) ))" ${f} 2>&1 > /dev/null
        then
            if [ "${verbose}" -ne "0" ]
            then
                echo "[${f}]"
                cat ${f}
            else
                echo "${f}"
            fi
        fi
    done
done