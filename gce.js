#!/usr/bin/env nodejs

var fs = require( 'fs' );
var util = require( 'util' );
var sprintf = util.format;

var _self_module = {
    load: function( fName ) {
        var o;

        o = JSON.parse( fs.readFileSync( fName, { "encoding": "UTF-8", "flag": "r" } ) );
        return( o );
    },

    testForRule0 : function ( o ) {
        var n;

        for( n = 0; n < o.tokens.length; ++n ) {
            if( o.tokens[ n ].dependencyEdge.label == 'ROOT' &&
                o.tokens[ n ].partOfSpeech.tag == 'VERB' &&
                o.tokens[ n ].lemma == 'ser' &&
                o.tokens[ n ].partOfSpeech.tense == 'PRESENT' &&
                o.tokens[ n ].partOfSpeech.person == 'THIRD' ) {
                // encontrado verbo SER como ROOT
                // deve ser seguido de artigo e pelo menos mais um token.
                if( n < o.tokens.length - 2 ) {
                    var DETtoken;

                    DETtoken = o.tokens[ n + 1 ];
                    if( DETtoken.partOfSpeech.tag == 'DET' )
                        return( n );
                }
            }
        };
        return( -1 );
    },

    findNextVerb : function ( o, start ) {
        var n;

        for( n = start + 1; n < o.tokens.length; ++n )
            if( o.tokens[ n ].partOfSpeech.tag == "VERB" )
                return( n );
        return( -1 );
    }
}

module.exports = _self_module;