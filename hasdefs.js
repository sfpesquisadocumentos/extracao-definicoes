
var fs = require( 'fs' );
var path = require( 'path' );
var conll = require( './conll' );
var gce = require( './gce' );

var printJSON = false;

function makeDef( o, tokenN, rule, fName )
{
    // refazer a getText para atuar apenas na sentenca referenciada (ver o.sentences[{text.content, text.beginOffset}]
    return( {
        "definiendum": conll.getText( o, 0, tokenN ),
        "definiens": {
            "text": conll.getText( o, tokenN, -1 ),
            "rule": rule,
            "src": fName
        }
    } );
}

function testJSONFile( fName )
{
    var o, tokenN;

    o = gce.load( fName );
    if( ( tokenN = gce.testForRule0( o ) ) >= 0 ) {
        if( printJSON )
            console.log( JSON.stringify( makeDef( o, tokenN, "linguistica", fName ) ) );
        return( 0 );
    }
    return( 1 );
}

var nArg;

for( nArg = 2; nArg < process.argv.length; ++nArg ) {
    switch( process.argv[ nArg ] ) {
    case "--json":
        printJSON = 1;
        break;

    case "--help":
    case "-h":
    case "-?":
        console.log( "%s [--json] gce.json", path.basename( process.argv[ 1 ] ) );
        console.log( "--json - imprime o registro da definicao na saida padrao (default nao)" );
        console.log( "gce.json = arquivo JSON de resultado do POS tagging de sentencas da Google Compute Engine." );
        process.exit( 3 );

    default:
        process.exit( testJSONFile( process.argv[ nArg ] ) );
        break;
    }
}

/*
    Codigos de retorno:
        0: arquivo possui uma definicao
        1: arquivo nao possui uma definicao
        2: arquivo nao informado
        3: apenas imprimir ajuda e encerrar
*/
process.exit( 2 );