#!/usr/bin/env nodejs

var fs = require( 'fs' );
var path = require( 'path' );
var conll = require( './conll' );
var gce = require( './gce' );

function testJSONFile( fName )
{
    var o, tokenN;

    o = gce.load( fName );
    if( ( tokenN = gce.testForRule0( o ) ) >= 0 ) {
        if( o.tokens[ tokenN + 2 ].partOfSpeech.tag == "NOUN" ) {
            var nextVerb;

            nextVerb = gce.findNextVerb( o, tokenN + 2 );
            if( nextVerb >= 0 ) {
                if( o.tokens[ nextVerb ].dependencyEdge.headTokenIndex == tokenN + 2 &&
                    o.tokens[ nextVerb ].lemma == "autorizar" ) {
                    return( 0 );
                }
            }
        }
    }
    return( 1 );
}

var nArg;

for( nArg = 2; nArg < process.argv.length; ++nArg ) {
    switch( process.argv[ nArg ] ) {
    case "--help":
    case "-h":
    case "-?":
        console.log( "%s gce.json", path.basename( process.argv[ 1 ] ) );
        console.log( "gce.json = arquivo JSON de resultado do POS tagging de sentencas da Google Compute Engine." );
        process.exit( 3 );

    default:
        process.exit( testJSONFile( process.argv[ nArg ] ) );
        break;
    }
}

/*
    Codigos de retorno:
        0: arquivo possui uma definicao
        1: arquivo nao possui uma definicao
        2: arquivo nao informado
        3: apenas imprimir ajuda e encerrar
*/
process.exit( 2 );