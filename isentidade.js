#!/usr/bin/env nodejs

var fs = require( 'fs' );
var path = require( 'path' );
var conll = require( './conll' );
var gce = require( './gce' );

function testJSONFile( fName )
{
    var o, tokenN;
    var ent;

    o = gce.load( fName );

    if( ( tokenN = gce.testForRule0( o ) ) >= 0 ) {
//        if( o.entities[ ent ].type == "ORGANIZATION" ) {
            for( ent = 0; ent < o.entities.length; ++ent ) {
                var mention;

                for( mention = 0; mention < o.entities[ ent ].mentions.length; ++mention ) {
                    if( o.entities[ ent ].mentions[ mention ].text.beginOffset < o.tokens[ tokenN ].text.beginOffset &&
                        o.entities[ ent ].mentions[ mention ].type == "PROPER" )
                        return( 0 );
                }
            }
//        }
    }
    return( 1 );
}

var nArg;

for( nArg = 2; nArg < process.argv.length; ++nArg ) {
    switch( process.argv[ nArg ] ) {
    case "--help":
    case "-h":
    case "-?":
        console.log( "%s gce.json", path.basename( process.argv[ 1 ] ) );
        console.log( "gce.json = arquivo JSON de resultado do POS tagging de sentencas da Google Compute Engine." );
        process.exit( 3 );

    default:
        process.exit( testJSONFile( process.argv[ nArg ] ) );
        break;
    }
}

/*
    Codigos de retorno:
        0: arquivo possui uma definicao
        1: arquivo nao possui uma definicao
        2: arquivo nao informado
        3: apenas imprimir ajuda e encerrar
*/
process.exit( 2 );