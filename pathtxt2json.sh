#!/usr/bin/env bash

# Exemplo:
# de:   /opt/normas/txt-sentencas/1947/LEI-1947-00127/00009-art3_cpt.txt
# para: /opt/normas/txt-sentencas-json/1947/LEI-1947-00127/00009-art3_cpt.json

sed "s/txt-sentencas/txt-sentencas-json/" | sed "s/\.txt$/\.json/"
